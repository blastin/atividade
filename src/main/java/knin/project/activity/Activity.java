package knin.project.activity;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * @param <E>
 * @param <C>
 */
public interface Activity<E, C> {

    <R> Activity<E, R> action(final Function<? super C, ? extends R> function);

    Activity<E, C> signal(final Consumer<? super C> consumer);

    ActivityGuard<E, C, C> guard(final Predicate<? super C> predicate);

    static <E> Activity<E, E> receive(final E e) {
        return builder().receive(e);
    }

    static ActivityBuilder builder() {
        return new ActivityBuilderImpl();
    }

    sealed interface ActivityBuilder permits ActivityBuilderImpl {

        ActivityBuilder withFactory(final ActivityFactory factory);

        <E> Activity<E, E> receive(final E e);

    }

    final class ActivityBuilderImpl implements ActivityBuilder {

        private ActivityBuilderImpl() {

            factory = new ActivityFactory() {

                @Override
                public <N, P> Activity<N, P> createActivity(final Activity<N, P> activity) {
                    return activity;
                }

                @Override
                public <N, P, Q> ActivityGuard<N, P, Q> createGuard(final ActivityGuard<N, P, Q> activityGuard) {
                    return activityGuard;
                }

            };

        }

        private ActivityFactory factory;

        @Override
        public ActivityBuilder withFactory(final ActivityFactory factory) {
            this.factory = Objects.requireNonNull(factory, "Factory cannot be null");
            return this;
        }

        @Override
        public <E> Activity<E, E> receive(final E e) {
            Objects.requireNonNull(e, "Entry Value cannot be null");
            return new FactorySealed(factory).createActivity(f -> new ActivityImpl<>(e, e, f));
        }

    }

    sealed interface Factory permits FactorySealed {

        <E, C> Activity<E, C> createActivity(final Function<? super Factory, ? extends Activity<E, C>> function);

        <E, C, D> ActivityGuard<E, C, D> createDecision
                (
                        final Function<? super Factory, ? extends ActivityGuard<E, C, D>> function
                );

    }

    final class FactorySealed implements Factory {

        private FactorySealed(final ActivityFactory factory) {
            this.factory = factory;
        }

        private final ActivityFactory factory;

        @Override
        public <N, P> Activity<N, P> createActivity
                (
                        final Function<? super Factory, ? extends Activity<N, P>> function
                ) {

            final class Proxy<E, C> implements Activity<E, C> {

                Proxy(final Activity<E, C> activity) {
                    this.activity = Objects.requireNonNull(activity, "activity cannot be null");
                }

                private final Activity<E, C> activity;

                @Override
                public <R> Activity<E, R> action(final Function<? super C, ? extends R> function) {
                    return activity.action(Objects.requireNonNull(function, "function cannot be null"));
                }

                @Override
                public Activity<E, C> signal(final Consumer<? super C> consumer) {
                    return activity.signal(Objects.requireNonNull(consumer, "consumer cannot be null"));
                }

                @Override
                public ActivityGuard<E, C, C> guard(final Predicate<? super C> predicate) {
                    return activity.guard(Objects.requireNonNull(predicate, "predicate cannot be null"));
                }

            }

            return new Proxy<>(factory.createActivity(function.apply(this)));

        }

        @Override
        public <N, P, Q> ActivityGuard<N, P, Q> createDecision(final Function<? super Factory, ? extends ActivityGuard<N, P, Q>> function) {

            final class Proxy<E, C, D> implements ActivityGuard<E, C, D> {

                Proxy(final ActivityGuard<E, C, D> activityGuard) {
                    this.activityGuard = Objects.requireNonNull(activityGuard, "activityGuard cannot be null");
                }

                private final ActivityGuard<E, C, D> activityGuard;

                @Override
                public <R> ActivityGuard<E, R, D> action(final Function<? super C, ? extends R> function) {
                    return activityGuard.action(Objects.requireNonNull(function, "function cannot be null"));
                }

                @Override
                public ActivityGuard<E, C, D> signal(final Consumer<? super C> consumer) {
                    return activityGuard.signal(Objects.requireNonNull(consumer, "consumer cannot be null"));
                }

                @Override
                public Activity<E, D> otherwise() {
                    return activityGuard.otherwise();
                }

            }

            return new Proxy<>(factory.createGuard(function.apply(this)));

        }

    }

}
