package knin.project.activity;

public interface ActivityFactory {

    <E, C> Activity<E, C> createActivity(final Activity<E, C> activity);

    <E, C, D> ActivityGuard<E, C, D> createGuard(final ActivityGuard<E, C, D> activityGuard);

}
