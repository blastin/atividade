package knin.project.activity;

import java.util.function.Consumer;
import java.util.function.Function;

/**
 * @param <E>
 * @param <C>
 * @param <D>
 */
public interface ActivityGuard<E, C, D> {

    <R> ActivityGuard<E, R, D> action(final Function<? super C, ? extends R> function);

    ActivityGuard<E, C, D> signal(final Consumer<? super C> consumer);

    Activity<E, D> otherwise();

}
