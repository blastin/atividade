package knin.project.activity;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * @param <E>
 * @param <C>
 */
final class ActivityImpl<E, C> implements Activity<E, C> {

    ActivityImpl(final E e, final C c, final Factory factory) {
        this.e = e;
        this.c = c;
        this.factory = factory;
    }

    private final E e;
    private final C c;

    private final Factory factory;

    @Override
    public <R> Activity<E, R> action(final Function<? super C, ? extends R> function) {
        return factory.createActivity(f -> new ActivityImpl<>(e, Objects.requireNonNull(function.apply(c)), f));
    }

    @Override
    public Activity<E, C> signal(final Consumer<? super C> consumer) {
        consumer.accept(c);
        return this;
    }

    @Override
    public ActivityGuard<E, C, C> guard(final Predicate<? super C> predicate) {

        if (predicate.test(c)) return factory.createDecision(f -> new Guard<>(e, c, c, f));

        return new GuardFail<>(factory, e, c);

    }

    private static final class Guard<E, C, D> implements ActivityGuard<E, C, D> {

        Guard(final E e, final C c, final D backup, final Factory factory) {
            this.e = e;
            this.c = c;
            this.backup = backup;
            this.factory = factory;
        }

        private final E e;
        private final C c;
        private final Factory factory;
        private final D backup;

        @Override
        public <R> ActivityGuard<E, R, D> action(final Function<? super C, ? extends R> function) {
            return factory.createDecision(f -> new Guard<>(e, Objects.requireNonNull(function.apply(c)), backup, f));
        }

        @Override
        public ActivityGuard<E, C, D> signal(final Consumer<? super C> consumer) {
            consumer.accept(c);
            return this;
        }

        @Override
        public Activity<E, D> otherwise() {
            return new ActivityLoop<>();
        }

        private static final class ActivityLoop<E, C> implements Activity<E, C> {

            @Override
            public <R> Activity<E, R> action(final Function<? super C, ? extends R> function) {
                return new ActivityLoop<>();
            }

            @Override
            public Activity<E, C> signal(final Consumer<? super C> consumer) {
                return this;
            }

            @Override
            public ActivityGuard<E, C, C> guard(final Predicate<? super C> predicate) {
                return new GuardLoop<>();
            }

            private static final class GuardLoop<E, C, D> implements ActivityGuard<E, C, D> {

                @Override
                public <R> ActivityGuard<E, R, D> action(final Function<? super C, ? extends R> function) {
                    return new GuardLoop<>();
                }

                @Override
                public ActivityGuard<E, C, D> signal(final Consumer<? super C> consumer) {
                    return this;
                }

                @Override
                public Activity<E, D> otherwise() {
                    return new ActivityLoop<>();
                }

            }
        }

    }

    private static final class GuardFail<E, C, D> implements ActivityGuard<E, C, D> {

        private GuardFail(final Factory factory, final E e, final D d) {
            this.factory = factory;
            this.e = e;
            this.d = d;
        }

        private final Factory factory;

        private final E e;

        private final D d;

        @Override
        public <R> ActivityGuard<E, R, D> action(final Function<? super C, ? extends R> function) {
            return new GuardFail<>(factory, e, d);
        }

        @Override
        public ActivityGuard<E, C, D> signal(final Consumer<? super C> consumer) {
            return this;
        }

        @Override
        public Activity<E, D> otherwise() {
            return factory.createActivity(f -> new ActivityImpl<>(e, d, f));
        }

    }

}
