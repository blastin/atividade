package knin.project.activity;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

class ActivityImplTest {

    @Test
    void shouldSendMessage() {

        final AtomicInteger atomic = new AtomicInteger(0);

        Activity
                .receive(List.of(1, 2, 3))
                .signal(a -> atomic.set(a.stream().reduce(0, Integer::sum)));

        Assertions.assertEquals(6, atomic.get());

    }

    @Test
    void shouldSendMessageWhenPredicateIsOk() {

        final AtomicInteger atomic = new AtomicInteger(0);

        Activity
                .receive(List.of(1, 2, 4))
                .action(list -> list.stream().max(Integer::compareTo).orElseThrow())
                .guard(inteiro -> (inteiro % 2) == 0)
                    .action(integer -> integer + 1)
                    .signal(atomic::set)
                .otherwise()
                    .signal(integer -> atomic.set(-1));

        Assertions.assertEquals(5, atomic.get());

    }

    @Test
    void shouldSendMessageWhenPredicareFail() {

        final AtomicInteger atomic = new AtomicInteger(0);

        Activity
                .receive(List.of(1, 2, 3))
                .action(list -> list.stream().max(Integer::compareTo).orElseThrow())
                .signal(atomic::set)
                .guard(inteiro -> (inteiro % 2) == 0)
                    .action(integer -> integer - 100)
                    .signal(atomic::set)
                    .action(integer -> Integer.toString(integer))
                    .signal(System.out::println)
                .otherwise()
                    .action(integer -> integer + 2)
                    .signal(atomic::addAndGet);

        Assertions.assertEquals(8, atomic.get());

    }

    @Test
    void shouldDoNothingWhenDecisionFail() {

        final AtomicInteger atomic = new AtomicInteger(0);

        Activity
                .receive(List.of(1, 2, 3))
                .action(list -> list.stream().max(Integer::compareTo).orElseThrow())
                .guard(inteiro -> inteiro > 3)
                    .signal(atomic::set);

        Assertions.assertEquals(0, atomic.get());


    }

    @Test
    void shouldSendAConsumerWhenInOtherwiseDecisionFail() {

        final AtomicInteger atomic = new AtomicInteger(0);

        Activity
                .receive(List.of(1, 2, 3))
                .action(list -> list.stream().max(Integer::compareTo).orElseThrow())
                .guard(integer -> integer > 4)
                    .action(integer -> integer - 100)
                    .signal(atomic::set)
                .otherwise()
                    .guard(integer -> integer > 3)
                        .action(integer -> integer - 200)
                        .signal(atomic::set)
                    .otherwise()
                        .signal(atomic::set);

        Assertions.assertEquals(3, atomic.get());

    }

    @Test
    void shouldSendAConsumerWhenInOtherwiseDecisionOk() {

        final AtomicInteger atomic = new AtomicInteger(0);

        Activity
                .receive(Stream.of("1", "2", "3").parallel())
                .action(stream -> stream.map(Integer::parseInt))
                .action(stream -> stream.max(Integer::compareTo).orElseThrow())
                .guard(integer -> integer > 4)
                    .action(integer -> integer - 100)
                    .signal(atomic::set)
                .otherwise()
                    .guard(integer -> integer > 2)
                        .action(integer -> integer - 200)
                        .signal(atomic::set)
                    .otherwise()
                        .signal(atomic::set);

        Assertions.assertEquals(-197, atomic.get());

    }

    @Test
    void withProxyLog() {

        final class LogActivity<E, C> implements Activity<E, C> {

            LogActivity(final Activity<E, C> activity) {
                this.activity = activity;
            }

            private final Activity<E, C> activity;

            @Override
            public <R> Activity<E, R> action(final Function<? super C, ? extends R> function) {
                System.out.println("Activity::action::INIT");
                final Activity<E, R> action = activity.action(function);
                System.out.println("Activity::action::OUT");
                return action;
            }

            @Override
            public Activity<E, C> signal(final Consumer<? super C> consumer) {
                System.out.println("Activity::send::INIT");
                final Activity<E, C> send = activity.signal(consumer);
                System.out.println("Activity::send::END");
                return send;
            }

            @Override
            public ActivityGuard<E, C, C> guard(final Predicate<? super C> predicate) {
                System.out.println("Activity::guard::INIT");
                final ActivityGuard<E, C, C> guard = activity.guard(predicate);
                System.out.println("Activity::guard::OUT");
                return guard;
            }

        }

        final class LogGuard<E, C, D> implements ActivityGuard<E, C, D> {

            LogGuard(final ActivityGuard<E, C, D> guard) {
                this.guard = guard;
            }

            private final ActivityGuard<E, C, D> guard;

            @Override
            public <R> ActivityGuard<E, R, D> action(final Function<? super C, ? extends R> function) {
                System.out.println("guard::action::INIT");
                final ActivityGuard<E, R, D> action = guard.action(function);
                System.out.println("guard::action::OUT");
                return action;
            }

            @Override
            public ActivityGuard<E, C, D> signal(final Consumer<? super C> consumer) {
                System.out.println("guard::consumer::INIT");
                final ActivityGuard<E, C, D> send = guard.signal(consumer);
                System.out.println("guard::consumer::OUT");
                return send;
            }

            @Override
            public Activity<E, D> otherwise() {
                System.out.println("guard::otherwise::INIT");
                final Activity<E, D> otherwise = guard.otherwise();
                System.out.println("guard::otherwise::OUT");
                return otherwise;
            }

        }

        final AtomicInteger atomic = new AtomicInteger(0);

        Activity
                .builder()
                .withFactory(new ActivityFactory() {

                    @Override
                    public <E, C> Activity<E, C> createActivity(final Activity<E, C> activity) {
                        return new LogActivity<>(activity);
                    }

                    @Override
                    public <E, C, D> ActivityGuard<E, C, D> createGuard(final ActivityGuard<E, C, D> guard) {
                        return new LogGuard<>(guard);
                    }

                })
                .receive(List.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10))
                .action(Collection::stream)
                .action(s -> s.reduce(0, Integer::sum))
                .guard(i -> i > 70)
                    .signal(atomic::set)
                    .action(integer -> integer - 10000)
                .otherwise()
                    .signal(System.out::println)
                    .guard(i -> i > 50)
                        .action(integer -> integer - 100)
                        .signal(atomic::set)
                    .otherwise()
                        .action(integer -> integer - 200)
                        .signal(atomic::set);

        Assertions.assertEquals(-45, atomic.get());

    }

    @Test
    void whenEntryValueIsNullShouldRaiseException() {
        Assertions.assertThrows(NullPointerException.class, () -> Activity.receive(null));
    }

    @Test
    void whenEntryFactoryNullSHouldRaiseException() {
        final Activity.ActivityBuilder builder = Activity.builder();
        Assertions.assertThrows(NullPointerException.class, () -> builder.withFactory(null));
    }

    @Test
    void whenConsumerIsNullShouldRaiseException() {
        final Activity<Integer, Integer> receive = Activity.receive(1);
        Assertions.assertThrows(NullPointerException.class, () -> receive.signal(null));
    }

    @Test
    void whenGuardIsNullShouldRaiseException() {
        final Activity<Integer, Integer> receive = Activity.receive(1);
        Assertions.assertThrows(NullPointerException.class, () -> receive.guard(null));
    }

    @Test
    void whenConsumerInGuardIsNullShouldRaiseException() {
        final ActivityGuard<Integer, Integer, Integer> guard = Activity.receive(3).guard(integer -> integer > 2);
        Assertions.assertThrows(NullPointerException.class, () -> guard.signal(null));
    }

    @Test
    void whenActionIsNullShouldRaiseException() {
        final Activity<Integer, Integer> receive = Activity.receive(3);
        Assertions.assertThrows(NullPointerException.class, () -> receive.action(null));
    }

    @Test
    void whenActionInGuardIsNullShouldRaiseException() {
        final ActivityGuard<Integer, Integer, Integer> guard = Activity.receive(3).guard(integer -> integer > 0);
        Assertions.assertThrows(NullPointerException.class, () -> guard.action(null));
    }

    @Test
    void whenActionValueIsNullShouldRaiseException() {
        final Activity<Integer, Integer> receive = Activity.receive(3);
        Assertions.assertThrows(NullPointerException.class, () -> receive.action(i -> null));
    }

    @Test
    void whenActionValueInGuardIsNullShouldRaiseException() {
        final ActivityGuard<Integer, Integer, Integer> guard = Activity.receive(3).guard(integer -> integer > 0);
        Assertions.assertThrows(NullPointerException.class, () -> guard.action(i -> null));
    }

    @Test
    void whenActivityInstanceIsNullShouldRaiseException() {

        final Activity.ActivityBuilder activityBuilder =
                Activity
                        .builder()
                        .withFactory(new ActivityFactory() {
                            @Override
                            public <E, C> Activity<E, C> createActivity(final Activity<E, C> activity) {
                                return null;
                            }

                            @Override
                            public <E, C, D> ActivityGuard<E, C, D> createGuard(final ActivityGuard<E, C, D> activityGuard) {
                                return activityGuard;
                            }
                        });

        Assertions.assertThrows(NullPointerException.class, () -> activityBuilder.receive(new int[]{1, 2}));

    }

    @Test
    void whenGuardInstanceIsNullShouldRaiseException() {

        final Activity<long[], long[]> receive =
                Activity
                        .builder()
                        .withFactory(new ActivityFactory() {
                            @Override
                            public <E, C> Activity<E, C> createActivity(final Activity<E, C> activity) {
                                return activity;
                            }

                            @Override
                            public <E, C, D> ActivityGuard<E, C, D> createGuard(final ActivityGuard<E, C, D> activityGuard) {
                                return null;
                            }
                        })
                        .receive(new long[]{1L, 2L});

        Assertions.assertThrows(NullPointerException.class, () -> receive.guard(l -> l[0] > 0L));

    }

    @Test
    void whenFactoryIsNullShouldRaiseException() {

        final Activity.ActivityBuilder builder = Activity
                .builder();

        Assertions.assertThrows(NullPointerException.class, () -> builder.withFactory(null));

    }

}